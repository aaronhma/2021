---
duration: 25
presentation_url:
room:
slot:
speakers:
- Jay Miller
title: "Making Government Useable by anyone with Pandas, Python and Eland"
type: talk
video_url:
---
Unreadable data can lead to bad perceptions and positive action being
delayed. Some of the most obscure data comes from reporting from local,
state and federal governments. That said the data is there, freely available
and it has to be provided in a consistent (at some level) manner.

This talk will look at how I made Police Call Records and Department of
Education Data Understandable easier to investigate with Python, Pandas and
Eland.
