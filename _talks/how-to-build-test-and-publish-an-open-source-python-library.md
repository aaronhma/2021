---
duration: 10
presentation_url:
room:
slot:
speakers:
- Simon Willison
title: "How to build, test and publish an open source Python library"
type: talk
video_url:
---
I currently maintain over 100 Python packages on PyPI. This is a lot! The
secret to managing this many packages is comprehensive automation powered by
GitHub Actions.

I'll show you how to use cookiecutter, pytest, GitHub Actions and
ReadTheDocs to develop a new Python library, add tests, run continuous
integration, build documentation and automatically  publish new releases of
your package to PyPI.
