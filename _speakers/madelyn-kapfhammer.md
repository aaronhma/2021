---
name: Madelyn Kapfhammer
talks:
- "Prioritizing Pandemic Rates with Python: Connecting generalized disease modeling to optimization in a pandemic world"
---
Hello! I'm Madelyn Kapfhammer, a current senior at Allegheny College. I am
combining my experiences in the Biology and Computer Science departments to
create interesting perspectives on the two differing fields. I am a Python
developer and blog writer with an avid interest in understanding how I can
solve complex biological and computational problems.
