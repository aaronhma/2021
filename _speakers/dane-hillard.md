---
name: Dane Hillard
talks:
- "Keeping code safe and modern with semantic searches"
---
I like leveraging software development skills for good. During my short
career I've had a chance to work in the fields of counterterrorism, cancer
research, and education. I'm an unrepentant foodie—read more about the
upcoming [Piquant](https://piquantmag.com/about) if you like. [I made a
music album](https://www.littleleviathan.com) once and [I make fashion and
portraiture photography](https://www.danehillard.com) here and there.
